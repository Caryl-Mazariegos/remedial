$(document).ready(function(){
  $( '.switch-yellow' ).click(function() {
      $( '.yellow' ).toggle(true);
      $( '.white' ).toggle(false);
      $( '.off' ).toggle(false);
      $( '.on' ).toggle(true);
  });
  $( '.switch-white' ).click(function() {
      $( '.yellow' ).toggle(false);
      $( '.white' ).toggle(true);
      $( '.off' ).toggle(false);
      $( '.on' ).toggle(true);
  });
  $( '.switch-off' ).click(function() {
      $( '.yellow' ).toggle(false);
      $( '.white' ).toggle(false);
      $( '.off' ).toggle(true);
      $( '.on' ).toggle(false);
  });
});



